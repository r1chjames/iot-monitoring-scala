package com.r1chjames.iot.monitoring.services

import java.util.UUID

import com.r1chjames.iot.monitoring.dao.DeviceDao
import com.r1chjames.iot.monitoring.entities.Device
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class DeviceService(@Autowired deviceDao: DeviceDao) {

  def getDeviceById(id: UUID): Device = {
    deviceDao.getDeviceById(id)
  }

  def getDeviceByIp(ip: String): Device = {
    deviceDao.getDeviceByIp(ip)
  }

  def addDevice(device: Device): Device = {
    deviceDao.addDevice(device)
  }

  def fullUpdateDevice(id: UUID, updatedDevice: Device): Device = {
    deviceDao.updateDevice(id, updatedDevice)
  }

  def partialUpdateDevice(id: UUID, updatedDevice: Device): Device = {
    deviceDao.updateDevice(id, updatedDevice)
  }

  def deleteDevice(id: UUID):Unit  = {
    deviceDao.deleteDevice(id)
  }





}
