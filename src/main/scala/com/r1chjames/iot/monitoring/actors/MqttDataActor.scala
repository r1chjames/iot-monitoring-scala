package com.r1chjames.iot.monitoring.actors

import akka.actor.Actor
import akka.event.Logging
import com.r1chjames.iot.monitoring.dao.InfluxDbDao
import com.r1chjames.iot.monitoring.entities.{InfluxDbRecord, MqttMsg}

class MqttDataActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case MqttMsg(topic, message) => {
      if (isDataMessage(topic)) {
        log.info("Receiving Data Message, Topic : %s, Message : %s".format(topic, message))
        val db = new InfluxDbDao
        db.connect("UC-FCSV")
        db.save(InfluxDbRecord(topic, message.toString))
        db.close()
      }
    }
  }


  def isDataMessage(topic: String): Boolean = {
    if (topic.contains("sensor")) true
    else false
  }
}
