package com.r1chjames.iot.monitoring.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.core.env.PropertySource
import org.springframework.stereotype.Component

import scala.beans.BeanProperty

//@Component
//@ConfigurationProperties("mqtt")
//@PropertySource("classpath:global.properties")
class Configuration(var host: String,
                         var port: Integer,
                         var topic: String,
                         var username: String,
                         var password: String) {
}
