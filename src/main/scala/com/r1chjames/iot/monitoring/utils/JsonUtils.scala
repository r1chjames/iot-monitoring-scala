package com.r1chjames.iot.monitoring.utils

import java.io.File

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper, SerializationFeature}
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule

object JsonUtils {

  val mapper = new ObjectMapper()

  mapper.registerModule(new JavaTimeModule())
  mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
  mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
  mapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false)
  mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)


  def writeJsonFile(path: String, value: Singleton): Unit = {
    mapper.writerWithDefaultPrettyPrinter().writeValue(new File(path), value)
  }

  def toJson(value: Singleton): String = {
    mapper.writerWithDefaultPrettyPrinter().writeValueAsString(value)
  }

  def fromJson[A](json: String, classType: Class[A]): Option[A] = {
    try {
      Option(mapper.readValue(json, classType))
    } catch {
      case e: Exception => Option.empty
    }
  }

}
