package com.r1chjames.iot.monitoring.entities

object DeviceStatusEnum extends Enumeration {
  val ONLINE, OFFLINE = Value
}
