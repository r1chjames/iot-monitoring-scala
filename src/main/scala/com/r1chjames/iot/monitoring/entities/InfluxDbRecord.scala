package com.r1chjames.iot.monitoring.entities

import java.time.LocalDateTime

case class InfluxDbRecord(
                           topic: String,
                           value: String,
                           timestamp: LocalDateTime = LocalDateTime.now()) extends NoSqlRecord {

}
