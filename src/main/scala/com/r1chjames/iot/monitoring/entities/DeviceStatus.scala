package com.r1chjames.iot.monitoring.entities

import java.time.LocalDateTime
import java.util.UUID

case class DeviceStatus(
                         id: UUID,
                         deviceId: UUID,
                         lastPolled: LocalDateTime,
                         status: String) {

}
