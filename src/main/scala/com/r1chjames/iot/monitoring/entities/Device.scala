package com.r1chjames.iot.monitoring.entities

import java.time.LocalDateTime
import java.util.UUID

import scala.beans.BeanProperty

case class Device(@BeanProperty id: UUID,
                  @BeanProperty name: String,
                  @BeanProperty deviceType: String,
                  @BeanProperty ip: String,
                  @BeanProperty lastUpdated: LocalDateTime,
                  @BeanProperty monitorType: MonitorType) {

}
