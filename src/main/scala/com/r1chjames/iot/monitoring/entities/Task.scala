package com.r1chjames.iot.monitoring.entities

import java.time.LocalDateTime
import java.util.{Locale, UUID}

import com.cronutils.descriptor.CronDescriptor
import com.cronutils.model.CronType
import com.cronutils.model.definition.CronDefinitionBuilder
import com.cronutils.parser.CronParser

case class Task(id: UUID, device: Device, cronExpression: String, lastUpdated: LocalDateTime) {

  private def descriptor  = CronDescriptor.instance(Locale.UK)
  private def parser = new CronParser(CronDefinitionBuilder.instanceDefinitionFor(CronType.QUARTZ))

  def validate(): Task = {
//    checkState(parser.parse(cronExpression) != null, "Bad cron expression " + cronExpression)
    this
  }

  def getReadableSchedule(): String = {
    descriptor.describe(parser.parse(cronExpression))
  }

}
