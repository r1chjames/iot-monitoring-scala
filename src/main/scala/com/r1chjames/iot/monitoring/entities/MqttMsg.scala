package com.r1chjames.iot.monitoring.entities

import org.eclipse.paho.client.mqttv3.MqttMessage

case class MqttMsg(topic: String, message: MqttMessage) {

}
