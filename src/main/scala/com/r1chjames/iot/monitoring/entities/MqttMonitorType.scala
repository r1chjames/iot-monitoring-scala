package com.r1chjames.iot.monitoring.entities

case class MqttMonitorType(
                            topic: String,
                            message: String,
                            interval: String) extends MonitorType(interval) {

}
