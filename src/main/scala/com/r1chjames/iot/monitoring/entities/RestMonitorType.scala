package com.r1chjames.iot.monitoring.entities

case class RestMonitorType(
                            uri: String,
                            method: String,
                            body: String,
                            contentType: String,
                            headers: Map[String, String],
                            interval: String) extends MonitorType(interval) {

}
