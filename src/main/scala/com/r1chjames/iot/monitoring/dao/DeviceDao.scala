package com.r1chjames.iot.monitoring.dao

;

import java.util
import java.util.UUID

import com.r1chjames.iot.monitoring.dao.mappers.DeviceMapper
import com.r1chjames.iot.monitoring.entities.Device
import org.jdbi.v3.sqlobject.customizer.{Bind, BindBean}
import org.jdbi.v3.sqlobject.statement.{SqlQuery, SqlUpdate, UseRowMapper};


trait DeviceDao {

  @UseRowMapper(classOf[DeviceMapper])
  @SqlQuery(
    "SELECT " +
      "d.id, d.name, d.type, d.ip, d.monitor_type, d.last_updated" +
      " FROM "
      + "devices d")
  def getAllDevices: util.ArrayList[Device]

  @UseRowMapper(classOf[DeviceMapper])
  @SqlQuery(
    "SELECT " +
      "d.id, d.name, d.type, d.ip, d.monitor_type, d.last_updated" +
      " FROM " +
      "devices d " +
      "WHERE " +
      "id = :id")
  def getDeviceById(@Bind("id") id: UUID): Device

  @UseRowMapper(classOf[DeviceMapper])
  @SqlQuery(
    "SELECT " +
      "d.id, d.name, d.type, d.ip, d.monitor_type, d.last_updated" +
      " FROM "
      + "devices d " +
      "WHERE " +
      "d.ip = :ip")
  def getDeviceByIp(@Bind("ip") ip: String): Device

  @UseRowMapper(classOf[DeviceMapper])
  @SqlQuery(
    "INSERT INTO " +
      "devices " +
      "(" + "name, type, ip, monitor_type " + ")" +
      "VALUES (" +
      ":device.name, :device.deviceType, :device.ip, :device.monitorType " +
      ") RETURNING *")
  def addDevice(@BindBean("device") device: Device): Device

  @UseRowMapper(classOf[DeviceMapper])
  @SqlQuery(
    "UPDATE " +
      "devices d " +
      "SET " +
      "d.name = COALESCE(:device.name, name), d.type = COALESCE(:device.deviceType, type), d.ip = COALESCE(:device.ip, ip), d.monitor_type = COALESCE(:device.monitorType, monitor_type) " +
      "WHERE d.id = :id " +
      "RETURNING *")
  def updateDevice(@Bind("id") id: UUID, @BindBean("device") device: Device): Device

  @SqlUpdate(
    "DELETE FROM " +
      "devices d " +
      "WHERE d.id = :id ")
  def deleteDevice(@Bind("id") id: UUID): Unit
}
