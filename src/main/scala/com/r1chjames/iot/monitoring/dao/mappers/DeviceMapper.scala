package com.r1chjames.iot.monitoring.dao.mappers

import java.sql.ResultSet
import java.util.UUID

import com.r1chjames.iot.monitoring.entities.{Device, MonitorType}
import com.r1chjames.iot.monitoring.utils.JsonUtils
import org.jdbi.v3.core.mapper.RowMapper
import org.jdbi.v3.core.statement.StatementContext

class DeviceMapper extends RowMapper[Device] {

  override def map(rs: ResultSet, ctx: StatementContext): Device = {
    Device(UUID.fromString(rs.getString("id")),
      rs.getString("name"),
      rs.getString("type"),
      rs.getString("ip"),
      rs.getTimestamp("last_updated").toLocalDateTime,
      JsonUtils.fromJson(rs.getString("monitor_type"), classOf[MonitorType]).orNull)
  }
}
