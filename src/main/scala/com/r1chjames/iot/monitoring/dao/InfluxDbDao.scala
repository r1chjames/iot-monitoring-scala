package com.r1chjames.iot.monitoring.dao

import com.paulgoldbaum.influxdbclient.{Database, InfluxDB, Point}
import com.r1chjames.iot.monitoring.entities.{InfluxDbRecord, NoSqlRecord}
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

class InfluxDbDao extends NoSqlDbInterface {
  val log = LoggerFactory.getLogger(classOf[InfluxDbDao])

  var database: Database = _

  override def connect(connectionString: String): Unit = {
    val influxdb = InfluxDB.connect(connectionString)
    database = influxdb.selectDatabase("mqtt_data")
    val result: Future[Boolean] = database.exists()
    if (!Await.result(result, Duration(5, "seconds"))) {
      database.create()
    }
  }

  override def save(record: NoSqlRecord): Unit = {
    val influxRecord = record.asInstanceOf[InfluxDbRecord]

    val point = Point(influxRecord.topic)
      .addTag("topic", influxRecord.topic)
      .addField("value", influxRecord.value)
      .addField("timestamp", influxRecord.timestamp.toString)

    save(point)
  }

  override def save(point: Point): Unit = {
    database.write(point)
    log.info("Record written: %s, %s, %s".format(point.key, point.tags, point.fields))
  }

  override def close(): Unit = {
    database.close()
  }

}
