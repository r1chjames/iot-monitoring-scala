package com.r1chjames.iot.monitoring.dao

import com.paulgoldbaum.influxdbclient.Point
import com.r1chjames.iot.monitoring.entities.NoSqlRecord

trait NoSqlDbInterface {

  def connect(connectionString: String): Unit

  def save(record: NoSqlRecord): Unit

  def save(point: Point): Unit

  def close(): Unit

}
