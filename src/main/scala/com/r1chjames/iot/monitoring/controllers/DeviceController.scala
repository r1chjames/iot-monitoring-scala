package com.r1chjames.iot.monitoring.controllers

import java.util.UUID

import com.r1chjames.iot.monitoring.entities.Device
import com.r1chjames.iot.monitoring.services.DeviceService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.web.bind.annotation._


@RestController
@RequestMapping(Array("/devices"))
class DeviceController() {

  @Autowired val deviceService: DeviceService = null

  @GetMapping(path = Array("/id/{id}"))
  def getDeviceInfoById(@PathVariable id: UUID): ResponseEntity[Device] = new ResponseEntity(deviceService.getDeviceById(id), HttpStatus.OK)

  @GetMapping(path = Array("/ip/{ip}"))
  def getDeviceInfoByIp(@PathVariable ip: String): ResponseEntity[Device] = new ResponseEntity(deviceService.getDeviceByIp(ip), HttpStatus.OK)

  @PostMapping(path = Array("/"))
  def addDevice(@RequestBody device: Device): ResponseEntity[Device] = new ResponseEntity(deviceService.addDevice(device), HttpStatus.ACCEPTED)

  @GetMapping(path = Array("/"))
  def hello(): ResponseEntity[String] = new ResponseEntity[String]("Hello", HttpStatus.OK)

}