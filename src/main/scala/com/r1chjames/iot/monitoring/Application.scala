package com.r1chjames.iot.monitoring

import com.r1chjames.iot.monitoring.configuration.Configuration
import com.r1chjames.iot.monitoring.dao.DeviceDao
import javax.sql.DataSource
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.sqlobject.SqlObjectPlugin
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean


@SpringBootApplication
class Application

object Application extends App {
  SpringApplication.run(classOf[Application])

  @Autowired
  @Bean
  def dbi(dataSource: DataSource): Jdbi = {
    Jdbi.create(dataSource)
      .installPlugin(new SqlObjectPlugin())
  }

  @Autowired
  @Bean
  def deviceDao(jdbi: Jdbi): DeviceDao = {
    jdbi.onDemand(classOf[DeviceDao])
  }
}
