CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE devices
(
  id                UUID PRIMARY KEY DEFAULT uuid_generate_v4() UNIQUE,
  name              CHARACTER VARYING,
  type              CHARACTER VARYING,
  ip                CHARACTER VARYING,
  last_updated      TIMESTAMP DEFAULT NOW(),
  monitor_type      JSONB
);
CREATE INDEX idx_devices_composite ON devices(id, ip);

CREATE TABLE device_status
(
  id                UUID PRIMARY KEY DEFAULT uuid_generate_v4() UNIQUE,
  device_id         UUID REFERENCES devices(id),
  last_polled       TIMESTAMP DEFAULT NOW(),
  status            CHARACTER VARYING
);
CREATE INDEX idx_device_status_composite ON device_status(device_id, status);