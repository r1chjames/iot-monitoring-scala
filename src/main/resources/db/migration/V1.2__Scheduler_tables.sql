CREATE TABLE scheduled_tasks
(
  id                UUID PRIMARY KEY DEFAULT uuid_generate_v4() UNIQUE,
  device_id         UUID REFERENCES devices(id),
  cron_expression   CHARACTER VARYING,
  last_updated      TIMESTAMP DEFAULT NOW()
);
