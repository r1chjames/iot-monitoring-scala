name := "iot-monitoring-scala"

version := "0.1"

scalaVersion := "2.12.6"

resolvers += Resolver.jcenterRepo

libraryDependencies ++= Seq(
  "org.springframework.boot" % "spring-boot-devtools" % "2.0.3.RELEASE",
  "org.springframework.boot" % "spring-boot-starter-data-rest" % "2.0.3.RELEASE",
  "org.springframework.boot" % "spring-boot-starter-jdbc" % "2.0.3.RELEASE",
  "org.flywaydb" % "flyway-core" % "5.0.3",
  "org.jdbi" % "jdbi3-core" % "3.1.1",
  "org.jdbi" % "jdbi3-sqlobject" % "3.1.1",
  "org.postgresql" % "postgresql" % "42.2.2",
  "org.json" % "json" % "20170516",
  "commons-io" % "commons-io" % "2.5",
  "org.apache.commons" % "commons-lang3" % "3.7",
  "org.quartz-scheduler" % "quartz" % "2.2.3",
  "com.cronutils" % "cron-utils" % "5.0.5",
  "org.slf4j" % "slf4j-api" % "1.7.5",

  "org.eclipse.paho" % "org.eclipse.paho.client.mqttv3" % "1.2.0",
  "com.typesafe.akka" %% "akka-actor" % "2.5.13",
  "com.paulgoldbaum" %% "scala-influxdb-client" % "0.6.0",


  "org.scalatest" %% "scalatest" % "3.2.0-SNAP10" % Test,
  "org.springframework.boot" % "spring-boot-starter-test" % "2.0.3.RELEASE" % Test,
  "info.cukes" % "cucumber-java8" % "1.2.5" % Test,
  "info.cukes" % "cucumber-picocontainer" % "1.2.5" % Test,
  "com.opentable.components" % "otj-pg-embedded" % "0.11.0" % Test,
  "com.typesafe.akka" %% "akka-testkit" % "2.5.13" % Test

)